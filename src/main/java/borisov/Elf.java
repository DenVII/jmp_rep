package borisov;

/**
 * Created by Denys_Iakibchuk on 7/24/2016.
 */
public class Elf {

    @InjectRandomInt(min=1, max=14)
    int stamina;

    @InjectRandomInt(min=10, max=25)
    int strength;

    @InjectRandomInt(min=50, max=150)
    int dexterity;





    public int getStamina() {
        return stamina;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public static void main(String[] args) {

        ObjectFactory objectFactory = ObjectFactory.getObjectFactory();

        for (int i = 0; i < 10; i++) {

            Elf elf = objectFactory.createObject(Elf.class);
            if (elf == null) {
                System.out.println("null");
                continue;
            }
            System.out.println("Elf" + (i+1) + " stamina: " + elf.getStamina() +  " dexterity: " + elf.getDexterity() + " strength: " + elf.getStrength());

        }
    }
}
