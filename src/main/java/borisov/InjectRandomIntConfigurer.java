package borisov;

import java.lang.reflect.Field;
import java.util.Random;

/**
 * Created by Denys_Iakibchuk on 7/24/2016.
 */
public class InjectRandomIntConfigurer implements ObjectConfigurer {

    private Random random = new Random();

    @Override
    public void configureObject(Object object) {

        Class<?> type = object.getClass();
        Field[] fields = type.getDeclaredFields();
        for (Field field: fields) {
            InjectRandomInt annotation = field.getAnnotation(InjectRandomInt.class);
            if (annotation != null) {

                if(field.getType() != Integer.TYPE) {
                    throw new RuntimeException("@Inject random int could be used only above int fields");
                }

                int min = annotation.min();
                int max = annotation.max();
                field.setAccessible(true);
                try {
                    field.set(object, min + random.nextInt(max-min));
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }

        }


    }
}
