package borisov;


public interface ObjectConfigurer {

    void configureObject(Object object);

}
