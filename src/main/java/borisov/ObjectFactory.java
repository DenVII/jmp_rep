package borisov;


import org.reflections.Reflections;

import java.io.Serializable;
import java.util.*;
public class ObjectFactory implements Serializable {

    private static ObjectFactory objectFactory = new ObjectFactory();
    private List<ObjectConfigurer> configurers = new LinkedList<>();
    private Reflections reflections;
    private Enum bds;

    private ObjectFactory() {
        reflections = new Reflections("borisov");
        Set<Class<? extends ObjectConfigurer>> classes = reflections.getSubTypesOf(ObjectConfigurer.class);
        for (Class<? extends ObjectConfigurer> configureClass : classes) {
            try {
                configurers.add(configureClass.newInstance());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static ObjectFactory getObjectFactory() {
        return objectFactory;
    }

    public <T> T createObject(Class<T> type) {

        T instance = null;

        try {
            instance = type.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        if (instance != null) {
            for (ObjectConfigurer configurer : configurers) {
                configurer.configureObject(instance);
            }
        }

        return instance;
    }

}
