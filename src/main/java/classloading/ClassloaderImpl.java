package classloading;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ClassloaderImpl  extends ClassLoader{

    private String pathToBin;

    public ClassloaderImpl() {
    }

    public void setPathToBin(String pathToBin) {
        this.pathToBin = pathToBin;
    }

    private byte[] getClassFromFS(String className) throws IOException{
        String replace = className.replace(".", "/");
        Path path = Paths.get(pathToBin, replace + ".class");
        return Files.readAllBytes(path);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        Class<?> classSource;

        try {
            byte[] classFromFS = getClassFromFS(name);
            classSource = defineClass("classloading.Semaphore", classFromFS, 0, classFromFS.length);
        } catch (IOException ex) {
            return super.findClass(name);
        }

        return classSource;
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {

        Class<?> aClass;
        try {
            if (name.contains("Semaphore")) {
                System.out.println("progress");
                aClass = findClass(name);
                System.out.println("success");
            } else {
                aClass = super.loadClass(name);
            }

        } catch (ClassNotFoundException ex) {
            System.out.println("error");
            aClass = super.loadClass(name);
        }

        return aClass;

    }

}
