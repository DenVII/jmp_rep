package classloading;

import java.lang.reflect.Method;
import java.util.Scanner;

/**
 * Created by Denys_Iakibchuk on 8/12/2016.
 */
public class Main {

    public static void main(String[] args) throws Exception {

//        if (args.length != 1) {
//            System.out.println("Program should have exactly one parameter");
//        }
            Scanner sc = new Scanner(System.in);

            for(;;) {
                ClassloaderImpl classloader = new ClassloaderImpl();
                try {
                    System.out.println("Enter class path: ");

                    if (sc.hasNext()) {
                        String path = sc.next();

                        System.out.println("progress");
                        classloader.setPathToBin(path);
                        Class<?> semaphore = classloader.loadClass("Semaphore");

                        Method[] methods = semaphore.getDeclaredMethods();
                        Object obj = semaphore.newInstance();

                        for (Method m : methods) {
                            m.invoke(obj);
                        }
                    }
                } catch (Exception ex) {
                    System.out.println("error ex");
                }

            }

    }
}
