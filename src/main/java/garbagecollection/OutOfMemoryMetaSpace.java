package garbagecollection;

import org.reflections.Reflections;

import java.util.Iterator;
import java.util.Set;


public class OutOfMemoryMetaSpace {

    public static StringBuilder sb = new StringBuilder("biggest string ever");

    public static void main(String[] args) {

        Reflections reflections = new Reflections("garbagecollection");
        Set<Class<? extends Object>> classes = reflections.getSubTypesOf(Object.class);

        Iterator<Class<?>> iterator = classes.iterator();


        while (iterator.hasNext()) {
            Class<?> next = iterator.next();
            System.out.println(next.getClass());

        }

    }

}


