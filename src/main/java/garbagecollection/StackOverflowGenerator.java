package garbagecollection;

import java.math.BigInteger;

public class StackOverflowGenerator {

    private static BigInteger bigInteger = BigInteger.valueOf(0);

    public void print() {

        System.out.println("" + bigInteger);
        bigInteger.add(BigInteger.ONE);
        print2();
    }

    public void print2() {

        System.out.println("" + bigInteger);
        bigInteger.add(BigInteger.ONE);
        print();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public static void main(String[] args) {


       StackOverflowGenerator stackOverflowGenerator = new StackOverflowGenerator();
        stackOverflowGenerator.print();


    }

}
