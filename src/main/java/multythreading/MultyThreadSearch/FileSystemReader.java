package multythreading.MultyThreadSearch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Constructor;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class FileSystemReader {

    private Logger log = LoggerFactory.getLogger(FileSystemReader.class);
    private Queue<String> folderPaths = new ConcurrentLinkedQueue<>();
    private Queue<String> filePaths = new ConcurrentLinkedQueue<>();
    private Queue<String> foundPaths = new ConcurrentLinkedQueue<>();
    private volatile String search;
    private int threadCount = 125;

    public static void main(String[] args) throws FileNotFoundException {

        String search = "DEBUG";
        String searchPath = "D:\\TDP\\BackDev";

        FileSystemReader fileSystemReader = new FileSystemReader();
        fileSystemReader.setSearch(search);
        fileSystemReader.setSearchPath(searchPath);
        fileSystemReader.setThreadCount(3);

        fileSystemReader.searchFiles();

    }

    public void setSearch(String search) {
        this.search = search;
    }

    public void setSearchPath(String searchPath) {
        folderPaths.add(searchPath);
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    private Thread[] startPool(Class clazz) {

        log.info("startPool started. Of class {}", clazz.getSimpleName());

        Thread[] pool = new Thread[threadCount];

            try {
                for (int i = 0; i < threadCount; i++) {
                    Constructor declaredConstructor = clazz.getDeclaredConstructor(this.getClass());
                    pool[i] = (Thread) declaredConstructor.newInstance(this);
                }

                for (int i = 0; i < threadCount; i++) {
                    pool[i].start();
                }
                log.info("Pool of class {} started.", clazz.getSimpleName());

            } catch (Exception e) {
                log.info("failed to instantiate threads of class: {}", clazz.getSimpleName());
                throw new RuntimeException("Failed to instantiate thread pool");
            }

        return pool;

    }

    public void searchFiles() {

        log.info("search files started search word is: {}, folder: {}", search);
        if (search == "" || (folderPaths.isEmpty() && filePaths.isEmpty())) {
            return;
        }
        long currentTimeMillis1 = System.currentTimeMillis();
        startPool(FileSearcherThread.class);

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            log.error("Main thread was interrupted");
        }


        Thread[] readerPool =  startPool(FilesReaderThread.class);

        try {
            for (int i = 0; i < readerPool.length; i++) {
                readerPool[i].join();
            }
            log.info("all threads are finished");
        } catch (InterruptedException e) {
            log.info("Thread is interrupted");
        }

        foundPaths.forEach(System.out::println);

        long currentTimeMillis2 = System.currentTimeMillis();
        log.info("Program ran for: {}", currentTimeMillis2 - currentTimeMillis1);


    }

    class FileSearcherThread extends Thread {

        private volatile boolean isWorking;

        public void setWorking(boolean working) {
            isWorking = working;
        }

        @Override
        public void run() {

            String path = folderPaths.poll();

            while (path != null) {
                getFilePath(path);
                path = folderPaths.poll();

                if(path == null) {
                    for (int i = 0; i < 3; i++) {
                        if(path != null) {
                            break;
                        }
                        try {
                            Thread.sleep(1000);
                            path = folderPaths.poll();
                        } catch (InterruptedException e) {
                            log.info("File search thread was interrupted: {}", this);
                        }
                    }
                }

            }

        }


        private void getFilePath(String path) {
            log.info("getFiles started. Path is: {}", path);
            File file = new File(path);
            if (file.exists()) {
                if (file.isDirectory()) {
                    if(file.isHidden()) {
                        return;
                    }
                    file.listFiles(new FileFilter() {
                        @Override
                        public boolean accept(File pathname) {
                            return false;
                        }
                    });
                    String[] list = file.list();
                    for (int i = 0; i < list.length; i++) {
                        String newPath = new String(path + "\\" + list[i]);
                        File current = new File(newPath);
                        if(current.isDirectory()) {
                            folderPaths.add(newPath);
                            log.info("Directory added: {}", newPath);
                        } else {
                            filePaths.add(newPath);
                            log.info("File added: {}", newPath);
                        }

                    }
                } else {
                    if(file.isHidden()) {
                        return;
                    }
                    filePaths.add(path);
                    log.info("File added: {}", path);
                }
            }
            log.info("getFiles finished.");
        }

    }

    class FilesReaderThread extends Thread {

        @Override
        public void run() {
            log.info("start run method thread {}", Thread.currentThread().getName());
            String path = filePaths.poll();

                while (path != null) {
                    File file = new File(path);
                    if (file.exists() && file.isFile()) {
                        readFile(file);
                    }

                    path = filePaths.poll();

                    if(path == null) {
                        for (int i = 0; i < 5; i++) {
                            if(path != null) {
                                break;
                            }
                            try {
                                Thread.sleep(1000);
                                path = filePaths.poll();
                            } catch (InterruptedException e) {
                                log.info("File search thread was interrupted: {}", this);
                            }
                        }
                    }


                }
            log.info("finished run method thread {}", Thread.currentThread().getName());
        }

        public void readFile(File file) {
            log.info("reading file {}", file);
            try (FileReader reader = new FileReader(file)) {
                char[] buffer = new char[(int) file.length()];
                reader.read(buffer);
                if (isHaveCoincidences(buffer)) {
                    foundPaths.add(file.getAbsolutePath());
                }
            } catch (FileNotFoundException e) {
                log.info("file do not exists {}", file);
            } catch (IOException ex) {
                log.info("could not read file {}", file);
            }
            log.info("read file finished. File: {}", file);
        }

        public boolean isHaveCoincidences(char[] buffer) {
            String str = new String(buffer);
            if (str.contains(search)) {
                return true;
            }
            return false;
        }

    }

}
