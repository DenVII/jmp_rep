package multythreading;


public class ThreadDemo {

    public static volatile String cache = "";

    public static void PrintMsg(String msg, Object monitor) {


        synchronized (System.out) {

            if(!cache.equals(msg)) {
                System.out.println(msg);
                cache = msg;
            } else {
                System.out.notifyAll();
            }
        }

    }


    public static void main(String[] args) throws InterruptedException {

        Object monitor = new Object();



        Thread t1 = new Thread(() -> {

            while (true) {
                synchronized (System.out) {

                    if(!cache.equals("Right")) {
                        System.out.println("Right");
                        cache = "Right";

                    } else {

                    }
                }
            }
        }, "Right");

        Thread t2 = new Thread(() -> {

            while (true) {
                synchronized (System.out) {

                    if(!cache.equals("Left")) {
                        System.out.println("Left");
                        cache = "Left";
                    }
                }
            }

        }, "Left");

        t1.start();
        t2.start();

    }

}

class MyThread extends Thread {

    @Override
    public void run() {
        System.out.println("My thread is running");
    }

}

