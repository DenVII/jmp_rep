package multythreading.cars;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Car implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(Car.class);
    private Long finish;
    private long friction;
    private long distance;
    private volatile WinnerCar winner;
    private volatile boolean disqualified;

    private String name;

    public Car(String name, long friction) {
        this.name = name;
        this.friction = friction;
    }

    public void setMaxDistance(Long finish) {
        this.finish = finish;
    }

    public String getName() {
        return name;
    }

    @Override
    public void run() {

        try {
            while (distance < finish && !Thread.currentThread().isInterrupted() && winner.getWinner() == null) {
                Thread.sleep(friction);

                if (disqualified) {
                    throw new InterruptedException();
                }

                distance += 100;
                log.info(name + " " + distance);

                if (distance >= finish) {
                    synchronized (winner) {
                        if(winner.getWinner() == null) {
                            winner.setWinner(this);
                            log.info(winner.toString());
                        }
                    }
                }
            }

        } catch (InterruptedException e) {
            log.info("The car was disqualified - " + name);
        }

    }

    public long getFriction() {
        return friction;
    }

    public void setWinner(WinnerCar winner) {
        this.winner = winner;
    }

    public void setDisqualified(boolean disqualified) {
        this.disqualified = disqualified;
    }

    @Override
    public String toString() {
        return getName();
    }
}