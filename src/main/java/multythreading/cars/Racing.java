package multythreading.cars;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Racing {

    public static void main(String[] args) {

        final Logger log = LoggerFactory.getLogger(Racing.class);

        ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

        WinnerCar winner = new WinnerCar();

        List<Car> cars = new LinkedList<Car>() {{
            add(new Car("Ferrari F12 Berlinetta", 45));
            add(new Car("Mercedes-Benz SLR McLaren", 45));
            add(new Car("Dodge Viper", 60));
            add(new Car("Lotus Exige S", 65));
            add(new Car("Lamburgini Murcielago", 55));
        }};

        cars.sort((Car car1, Car car2) -> {
            if (car1.getFriction() >= car2.getFriction()) {
                return 1;
            } else {
                return -1;
            }
        });

        ArrayList<Thread> racingCars = new ArrayList<Thread>();
        Thread ferrari = new Thread(cars.get(0), cars.get(0).getName());
        Thread mercedes = new Thread(cars.get(1), cars.get(1).getName());
        Thread dodge = new Thread(cars.get(2), cars.get(2).getName());
        Thread lotus = new Thread(cars.get(3), cars.get(3).getName());
        Thread lamburgini = new Thread(cars.get(4), cars.get(4).getName());

        racingCars.add(ferrari);
        racingCars.add(mercedes);
        racingCars.add(dodge);
        racingCars.add(lotus);
        racingCars.add(lamburgini);

        for (int i = 0; i < racingCars.size(); i++) {
            Car car = cars.get(i);
            car.setMaxDistance(10000L);
            car.setWinner(winner);
        }

        for (int i = 0; i < racingCars.size(); i++) {
            new Thread(cars.get(i)).start();
        }


        Thread disqualifier = new Thread(() -> {
            Car disqualifiedCar = null;
            Thread disqualify = null;
            while (true) {
                try {
                    Thread.sleep(2000);

                    synchronized (winner) {
                        if (winner.getWinner() == null && cars.size() > 0) {
                            int carId = new Random().nextInt(racingCars.size());
                            disqualifiedCar = cars.get(carId);
                            disqualifiedCar.setDisqualified(true);
                            log.info("Interrupting car " + disqualifiedCar);
                        } else {
                            break;
                        }
                    }
                } catch (InterruptedException e) {
                    log.info(e.getMessage());
                }
            }

        });

        disqualifier.start();
    }

}
