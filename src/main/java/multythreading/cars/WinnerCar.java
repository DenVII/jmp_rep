package multythreading.cars;


public class WinnerCar {

    volatile Car winner;

    public void setWinner(Car winner) {
        this.winner = winner;
    }

    public Car getWinner() {
        return winner;
    }

    @Override
    public String toString() {
        return "The winner is" + winner;
    }
}
