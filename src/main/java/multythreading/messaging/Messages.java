package multythreading.messaging;


public class Messages {

    private Object lock = new Object();

    public Object getLock() {
        return lock;
    }

    public static void main(String[] args) {

        final Object lock = new Object();

        Thread t1 = new Thread(()-> {

            while (true) {

                synchronized (lock) {
                    lock.notify();
                    System.out.println("Right");
                    try {
//                        while (!Thread.currentThread().isInterrupted()) {
                            lock.wait();
//                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }


        }, "ThreadRight");


        Thread t2 = new Thread(()-> {

            while (true) {
                synchronized (lock) {
                    lock.notify();
                    System.out.println("Left");
                    try {
//                        while (!Thread.currentThread().isInterrupted()) {
                            lock.wait();
//                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }, "ThreadLeft");


        t1.start();
        t2.start();

    }

}
