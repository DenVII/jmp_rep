package multythreading.threadPool;

import java.util.ArrayDeque;
import java.util.Collection;

/**
 * Created by Denys_Iakibchuk on 8/20/2016.
 */
public class MyBlockingQueue<T>{

    ArrayDeque<T> collection = new ArrayDeque<>();

    public MyBlockingQueue() {
    }

    public synchronized boolean add(T o) {
        return collection.add(o);
    }

    public synchronized T poll() {
        return collection.poll();
    }

    public synchronized void addAll(Collection<T> col) {
        collection.addAll(col);
    }

}
