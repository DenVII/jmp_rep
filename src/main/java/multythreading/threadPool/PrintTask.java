package multythreading.threadPool;


public class PrintTask implements Task {

    private String taskMsg;
    private long delay = 500L;

    public PrintTask(String taskMsg, Long delay) {
        this.taskMsg = taskMsg;
        this.delay = delay;
    }

    public PrintTask(String taskMsg) {
        this.taskMsg = taskMsg;
    }

    @Override
    public long getDelay() {
        return delay;
    }

    @Override
    public void doTask() {
        System.out.println(taskMsg);
    }
}
