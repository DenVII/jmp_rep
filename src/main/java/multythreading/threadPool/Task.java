package multythreading.threadPool;

public interface Task {

    public void doTask();

    public long getDelay();
}
