package multythreading.threadPool;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class TaskExecutor {

    private MyBlockingQueue<Task> tasks = new MyBlockingQueue<>();
    private int threadNumber = 2;
    private Thread[] threadPool;


    public TaskExecutor(Collection<Task> tasks) {
        this.tasks.addAll(tasks);
    }

    public void setThreadNumber(int threadNumber) {
        this.threadNumber = threadNumber;
    }

    public void execute() {
        createThreads();
        for (int i = 0; i < threadNumber; i++) {
            threadPool[i].start();
        }
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    private void createThreads() {
        System.out.println("Creating two threads");
        threadPool = new Thread[threadNumber];

        for (int i = 0; i < threadNumber; i++) {
            threadPool[i] = new Thread(() -> {

                while(true) {

                    Task task = tasks.poll();
                    if(task != null) {
                        System.out.println("Thread with id: " + Thread.currentThread().getName() + " started.");
                        try {
                            Thread.sleep(task.getDelay());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            tasks.add(task);
                            continue;
                        }
                        task.doTask();
                        System.out.println("Thread with id: " + Thread.currentThread().getName() + " finished.");
                    } else {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }

            }, "" + (i + 1));
        }

    }

    public static void main(String[] args) {

        List<Task> tasks = new LinkedList<Task>() {{
            add(new PrintTask("I am eating"));
            add( new PrintTask("I am drinking"));
            add(new PrintTask("I am digging"));
            add(new PrintTask("I am kissing"));
            add(new PrintTask("I am having rest"));
            add(new PrintTask("I am sleeping"));
            add(new PrintTask("I am wondering around"));
            add(new PrintTask("I am learning"));
        }};


        TaskExecutor taskExecutor = new TaskExecutor(tasks);
        taskExecutor.setThreadNumber(4);
        taskExecutor.execute();

        for (int i = 0; i < 10; i++) {
            taskExecutor.addTask(new PrintTask("I am making exercise number " + (i + 1)));
        }

    }

}


