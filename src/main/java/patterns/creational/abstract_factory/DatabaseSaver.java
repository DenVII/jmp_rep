package patterns.creational.abstract_factory;

import java.sql.*;

/**
 * Created by Denys_Iakibchuk on 7/23/2016.
 */
public class DatabaseSaver implements Saver {

    private Connection connection;
    private static final String WRITE_QUERY = "Insert into Person";
    private static final String READ_QUERY = "Select * Person";
    private static final String READ_BY_NAME = "Select * Person";


    public void setConnection(Driver driver, String url) {

        try {
            DriverManager.registerDriver(driver);
            this.connection = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println("Connection was not established");
            e.printStackTrace();
        }
    }

    public boolean isConnectionEstablished() {
        if(this.connection == null) {
            System.out.println("You have to establish connection");
            return false;
        }

        return true;
    }


    @Override
    public void writePerson(Person person) {

        if(isConnectionEstablished()) {

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(WRITE_QUERY);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public Person read() {

        if(isConnectionEstablished()) {

            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(READ_QUERY);
                int row = resultSet.getRow();
                String name = resultSet.getString(1);
                String address = resultSet.getString(2);
                return new Person(name, address);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    public Person read(String userName) {

        if(isConnectionEstablished()) {

            try {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(READ_BY_NAME);
                int row = resultSet.getRow();
                String name = resultSet.getString(1);
                String address = resultSet.getString(2);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
