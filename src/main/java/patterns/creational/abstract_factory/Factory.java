package patterns.creational.abstract_factory;


public interface Factory {


    Saver createSaver();

}
