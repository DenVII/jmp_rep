package patterns.creational.abstract_factory;

/**
 * Created by Denys_Iakibchuk on 7/23/2016.
 */
public class FileFactory implements Factory {
    @Override
    public Saver createSaver() {
        return new FileSaver();
    }
}
