package patterns.creational.abstract_factory;

import java.io.*;

/**
 * Created by Denys_Iakibchuk on 7/23/2016.
 */
public class FileSaver implements Saver {

    private File file;

    public void setFile(File file) {
        this.file = file;
    }

    private boolean isFileExists() {
        return file != null && file.exists();
    }


    @Override
    public void writePerson(Person person) {

        if (isFileExists()) {
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(this.file));
                bufferedWriter.write(person.getName() + "\n " + person.getAddress());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public Person read() {
        if (isFileExists()) {
            try {
                BufferedReader bufferedReader= new BufferedReader(new FileReader(file));

                String name = bufferedReader.readLine();
                String address = bufferedReader.readLine();

                return new Person(name,address);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    public Person read(String name) {
        throw new RuntimeException();
    }
}
