package patterns.creational.abstract_factory;

/**
 * Created by Denys_Iakibchuk on 7/23/2016.
 */
public class Person {

    private String name;
    private String address;

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public Person(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
