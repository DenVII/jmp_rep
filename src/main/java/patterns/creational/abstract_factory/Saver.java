package patterns.creational.abstract_factory;


public interface Saver {

    void writePerson(Person person);
    Person read();
    Person read(String name);

}
