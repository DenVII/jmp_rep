package patterns.creational.builder;

import java.util.Date;

public final class Person {

    private final String name;
    private final Date birthDate;
    private final String streetAddress;
    private final int age;

    private Person(String name, Date birthDate, String streetAddress, int age) {

        this.age = age;
        this.streetAddress = streetAddress;
        this.birthDate = birthDate;
        this.name = name;

    }

    public String getName() {
        return name;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public int getAge() {
        return age;
    }

    public Date getBirthDate() {
        return new Date(birthDate.getTime());
    }

    public static class Builder {

        private String name;
        private Date birthDate;
        private String streetAddress;
        private int age;

        private Builder() {
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder birthDate(Date birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public Builder streetAddress(String streetAddress) {
            this.streetAddress = streetAddress;
            return this;
        }

        public Builder age(int age) {
            this.age = age;
            return this;
        }

        private void checkForEmptyField() {
            if (name.isEmpty() || streetAddress.isEmpty() || birthDate == null || age < 1) {
                throw new RuntimeException();
            }
        }

        private void resetFields() {
            name = "";
            streetAddress = "";
            age = 0;
            birthDate = null;
        }

        public Person build() {

            checkForEmptyField();
            Person person = new Person(name, birthDate, streetAddress, age);
            resetFields();
            return person;

        }

    }


}
