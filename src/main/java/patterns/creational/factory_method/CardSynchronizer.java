package patterns.creational.factory_method;


import java.util.HashMap;
import java.util.Map;

public abstract class CardSynchronizer {

    private CustomerCard customerCard;

    private String keyName = "name";
    private String keyAddress = "address";

    public CustomerCard readData() {

        Map dataMap = readCard();
        Object name = dataMap.get(keyName);
        Object address = dataMap.get(keyAddress);

        if (name == null || address == null) {
            throw new UnsupportedOperationException();
        }

        return new CustomerCard(new Person((String)name, new Address((String)address)));

    }

    public void writeData() {
        Map<String, String> map = new HashMap<String, String>() {{
            put(keyName, customerCard.getCardholderName());
            put(keyAddress, customerCard.getCardHolderAddress());
        }};
        writeCard(map);
    }

    public void setCustomerCard(CustomerCard customerCard) {
        this.customerCard = customerCard;
    }

    protected abstract void writeCard(Map card);

    protected abstract Map<String, String> readCard();


}
