package patterns.creational.factory_method;


public class CustomerCard {

    private Person cardholder;

    public CustomerCard(Person cardholder) {
        this.cardholder = cardholder;
    }

    public String getCardholderName() {
        return cardholder.getName();
    }

    public String getCardHolderAddress() {
        return cardholder.getAddress().toString();
    }


}
