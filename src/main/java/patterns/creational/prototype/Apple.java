package patterns.creational.prototype;

import java.util.Date;

public class Apple implements Cloneable {

    private String type;
    private Date date;

    public void setType(String type) {
        this.type = type;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    protected Apple clone() throws CloneNotSupportedException {

        Apple copy = (Apple) super.clone();
        copy.setDate(new Date());
        return copy;

    }
}
