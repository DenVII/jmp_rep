package patterns.creational.singletone;

import patterns.creational.factory_method.CardSynchronizer;

import javax.sql.DataSource;
import java.util.Map;

public class DatabaseCardSynchronizer extends CardSynchronizer {

    private static final DatabaseCardSynchronizer databaseCardSynchronizer = new DatabaseCardSynchronizer();

    private DatabaseCardSynchronizer() {}

    private DataSource dataSource;

    public DatabaseCardSynchronizer getInstance(DataSource dataSource) {
        setDataSource(dataSource);
        return databaseCardSynchronizer;
    }

    private void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    protected void writeCard(Map card) {
        //TO DO
        // write to database
    }

    @Override
    protected Map<String, String> readCard() {
        //TO DO
        //read from database
        return null;
    }
}
