package patterns.creational.singletone;

import patterns.creational.factory_method.CardSynchronizer;

import java.io.File;
import java.util.Map;

/**
 * Created by Denys_Iakibchuk on 7/23/2016.
 */
public class FileSystemCardSynchronizer extends CardSynchronizer {

    private File file;
    private static volatile FileSystemCardSynchronizer fileSystemCardSynchronizer;

    private FileSystemCardSynchronizer() {
    }

    public static FileSystemCardSynchronizer getInstance(File file) {

        if (fileSystemCardSynchronizer == null) {
            synchronized(FileSystemCardSynchronizer.class) {
                if (fileSystemCardSynchronizer == null) {
                    fileSystemCardSynchronizer = new FileSystemCardSynchronizer();
                    fileSystemCardSynchronizer.setFile(file);
                    return fileSystemCardSynchronizer;
                }
            }
        }
        fileSystemCardSynchronizer.setFile(file);
        return fileSystemCardSynchronizer;

    }


    private void setFile(File file) {
        this.file = file;
    }

    @Override
    protected void writeCard(Map card) {
        //TO DO
        // write to file system
    }

    @Override
    protected Map<String, String> readCard() {
        //TO DO
        // read from file system
        return null;
    }
}
