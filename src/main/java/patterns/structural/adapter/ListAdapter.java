package patterns.structural.adapter;

import java.util.List;

/**
 * Adapter for any List collection class
 */
public class ListAdapter<E> implements Stack<E> {

    private List<E> adaptee;
    private int index;

    public ListAdapter(List<E> list) {
        this.adaptee = list;
        this.index = list.size() - 1;
    }

    @Override
    public void push(E e) {

        adaptee.add(e);
        index++;

    }

    @Override
    public E pop()  {

        if(index<0) {
            return null;
        }

        E e = adaptee.get(index);
        adaptee.remove(index);
        index--;
        return e;

    }

    @Override
    public int size() {
        return adaptee.size();
    }

}
