package patterns.structural.adapter;


import java.util.LinkedList;

public class Main {



    public static void main(String[] args) {

        ListAdapter<String> listAdapter = new ListAdapter<>(new LinkedList<String>());
        listAdapter.push("First");
        listAdapter.push("Second");

        System.out.println(listAdapter.pop());
        System.out.println(listAdapter.pop());
        System.out.println(listAdapter.pop());

    }

}
