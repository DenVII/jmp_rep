package patterns.structural.adapter;

/**
 * Interface for stack methods
 */
public interface Stack<E> {

    public void push(E e);

    public E pop();

    public int size();

}
