package patterns.structural.composite.FileSystem;

import java.util.HashSet;
import java.util.Set;

/**
 * Directory class represents folder in file system
 */
public class Directory implements FSEntity {

    private Set<FSEntity> fileEntities;
    private String name;


    public Directory(String name) {
        this.name = name;
        this.fileEntities = new HashSet<FSEntity>();
    }

    public boolean add(FSEntity entity) {
        return fileEntities.add(entity);
    }

    public boolean remove(FSEntity entity) {
        return fileEntities.remove(entity);
    }

    public void list() {

        for(FSEntity entity : fileEntities) {

            if(entity instanceof Directory) {
                System.out.println("Directory name: " + entity.name() + " " + entity.size() + " KB");
                ((Directory) entity).list();
            } else {
                System.out.println("        File name: " + entity.name() + " " + entity.size() + " KB");
            }

        }

    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public int size() {

        int size = 0;

        for(FSEntity entity : fileEntities) {
            size += entity.size();
        }

        return size;
    }
}
