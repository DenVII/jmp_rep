package patterns.structural.composite.FileSystem;

/**
 * Describes method for file(catalog) properties
 */
public interface FSEntity {

    public String name();

    public int size();

}
