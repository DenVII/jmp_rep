package patterns.structural.composite.FileSystem;

/**
 * Created by Denys_Iakibchuk on 7/14/2016.
 */
public class File implements FSEntity {

    private String name;
    private int size = 0;

    public File(String name, int size) {
        this.name = name;
        this.size = size;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public int size() {
        return size;
    }
}
