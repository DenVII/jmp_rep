package patterns.structural.composite.Parser;

/**
 * Created by Denys_Iakibchuk on 7/14/2016.
 */
public class Attribute  {

    private String name;
    private String value;

    public Attribute(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
