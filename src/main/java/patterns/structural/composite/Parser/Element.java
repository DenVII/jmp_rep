package patterns.structural.composite.Parser;

import java.util.ArrayList;
import java.util.List;

public class Element {

    private String name;
    private List<Element> elements = new ArrayList<>();
    private List<Attribute> attributes = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void print() {

        System.out.println("Element: " + name);

        StringBuilder attrString = new StringBuilder();

        for(Attribute attribute: attributes) {
            attrString.append(attribute.getName());
            attrString.append(" = ");
            attrString.append(attribute.getValue());
            attrString.append(" ");
        }

        if(attrString.length() != 0) {
            System.out.println("Attributes: " + attrString.toString());
        }

        for(Element element: elements) {
            element.print();
        }
    }

    public int getElementsCount() {
        int count = elements.size();

        for(Element element: elements) {
            count += element.getElementsCount();
        }

        return count;
    }



    public Element(String name) {
        this.name = name;
    }

    public void addElement(Element element) {
        elements.add(element);
    }

    public void addAttribute(Attribute attribute) {
        attributes.add(attribute);
    }

}
