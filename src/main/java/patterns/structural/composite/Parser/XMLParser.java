package patterns.structural.composite.Parser;


import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class XMLParser {

    private Document doc = null;

    public void readFile(File file) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Element createMemoryStructure() {
        return createMemoryStructure(doc.getDocumentElement());
    }

    private Element createMemoryStructure(Node node) {

        if(node == null) {
            return null;
        }

        Element element = new Element(node.getNodeName());

        NamedNodeMap attributes = node.getAttributes();

        for(int j=0; j<attributes.getLength(); j++) {
            Node attribute = attributes.item(j);
            Attribute attr = new Attribute(attribute.getNodeName(), attribute.getNodeValue());
            element.addAttribute(attr);
        }


        NodeList childNodes = node.getChildNodes();

        for(int i = 0; i < childNodes.getLength(); i++) {

            if(childNodes.item(i).getNodeType() == Node.ELEMENT_NODE) {

                Element childElement = createMemoryStructure(childNodes.item(i));

                if(childElement != null) {
                    element.addElement(childElement);
                }

            }

        }

        return element;

    }

}
