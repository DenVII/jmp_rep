package patterns.structural.decorator;


import java.io.*;

public class PersonInputStream extends FilterInputStream {

    private InputStream inputStream;

    public PersonInputStream(InputStream in) {

        super(in);
        this.inputStream = in;

    }

    public String readPerson() throws IOException {


        int c = super.read();
        StringBuilder sb = new StringBuilder();
        while(c != -1) {
            sb.append((char)c);
            c = super.read();
        }

        if(sb.length() != 0) {
            sb.setCharAt(0, Character.toLowerCase(sb.charAt(0)));
        }

        return sb.toString();

    }

}
