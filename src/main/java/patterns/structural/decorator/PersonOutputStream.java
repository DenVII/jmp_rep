package patterns.structural.decorator;


import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class PersonOutputStream extends FilterOutputStream {

    private volatile OutputStream outputStream;

    public PersonOutputStream(OutputStream out) {
        super(out);
        this.outputStream = out;
    }

    public void writePerson(String person) throws IOException {
        char firstChar = person.charAt(0);
        person = person.replace(firstChar, Character.toLowerCase(firstChar));
        super.write(person.getBytes());
    }

}
