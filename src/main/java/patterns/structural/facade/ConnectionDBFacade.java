package patterns.structural.facade;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDBFacade {

    private Connection connection;
    private User defaultUser;
    private String connectionName;
    private String currentDB;
    private String currentTable;

    public ConnectionDBFacade(String connectionName, String driverName) throws SQLException, ClassNotFoundException {
        Class.forName(driverName);
        this.connectionName = connectionName;
        defaultUser = new User("User", "11111");
    }

    public void createDB(String dbName) throws SQLException {
        createDB(dbName, defaultUser);
    }

    public void createDB(String dbName, User user) {

        if(user == null) {
            user = defaultUser;
        }

        try {
            connection = DriverManager.getConnection("",user.getName(), user.getPassword());
            connection.createStatement().execute("");
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e1) {
                    // TO DO
                }
            }
            throw new RuntimeException("Could not create database!", e);
        }

    }

    public void createTable() {

    }

}
