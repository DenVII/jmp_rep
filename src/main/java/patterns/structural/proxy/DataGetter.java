package patterns.structural.proxy;


import patterns.creational.abstract_factory.Person;

public interface DataGetter {

    Person readPerson(String name);

}
