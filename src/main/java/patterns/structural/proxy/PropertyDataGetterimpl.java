package patterns.structural.proxy;


import patterns.creational.abstract_factory.Person;

/**
 * Created by Denys_Iakibchuk on 8/14/2016.
 */
public class PropertyDataGetterimpl implements DataGetter {

    @Override
    public Person readPerson(String name) {
        String prop = System.getProperty(name);
        if(prop == null || name == null) {
            return null;
        } else {
            return new Person(name,prop);
        }
    }
}
