package patterns.structural.proxy;

import patterns.creational.abstract_factory.Person;

import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

/**
 * Class that delegates readPerson function to any
 * implementation of DataGetter interface. Provides
 * caching for retreived objects.
 */
public class ProxyPropertyGetter implements DataGetter {

    private WeakHashMap<String, WeakReference<Person>> personsCache;
    private DataGetter dataGetterImpl;

    public ProxyPropertyGetter(DataGetter dataGetterImpl) {
        this.dataGetterImpl = dataGetterImpl;
        personsCache = new WeakHashMap<String, WeakReference<Person>>();

    }

    @Override
    public Person readPerson(String name) {

        if(personsCache.get(name) != null) {
            return personsCache.get(name).get();
        }

        Person person = dataGetterImpl.readPerson(name);
        if(person != null) {
            personsCache.put(name, new WeakReference<Person>(person));
        }

        return person;

    }
}
