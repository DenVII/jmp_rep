package troubleshooting.bottleneck;

/**
 * Created by Denys_Iakibchuk on 8/28/2016.
 */
public class Incrementor {

    public static volatile int field;
    public static Object lock = new Object();

    public static void main(String[] args) {

        Thread[] threadPool = new Thread[4];

        for (int i = 0; i < 4; i++) {

            threadPool[i] = new Thread(() -> {
                System.out.println("Thread: " + Thread.currentThread().getName() + " started working");
                while (true) {
                    synchronized (lock) {

                        field++;
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    Thread.yield();
                }
            }, ("Thread" + i));

        }


        for (int i = 0; i < 4; i++) {
            threadPool[i].start();
        }


    }


}
