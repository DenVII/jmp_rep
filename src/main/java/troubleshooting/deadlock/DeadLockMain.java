package troubleshooting.deadlock;

/**
 * Created by Denys_Iakibchuk on 8/27/2016.
 */
public class DeadLockMain {

    public static Object lock1 = new Object();
    public static Object lock2 = new Object();
    public static Object lock3 = new Object();
    public static Object lock4 = new Object();


    public static void printHello() {

        synchronized (lock1) {
            System.out.println("Hello, World!");
            printHi();
        }
    }

    public static void printHi() {
        synchronized (lock2) {
            System.out.println("HI, World!");
            printCio();
        }
    }


    public static void printCio() {
        synchronized (lock3) {
            System.out.println("Cio, World!");
            printBuonGiorno();
        }
    }

    public static void printBuonGiorno() {
        synchronized (lock4) {
            System.out.println("Buon giorno, World!");
            printHello();
        }
    }

    public static void main(String[] args) throws InterruptedException {

        Thread helloThread = new Thread(() -> {
            printHello();
        }, "Hello thread");

        Thread hiThread = new Thread(() -> {
            printHi();
        }, "Hi thread");

        Thread cioThread = new Thread(() -> {
            printCio();
        }, "Cio thread");

        Thread buonGiornoThread = new Thread(() -> {
            printBuonGiorno();
        }, "Buon giorno thread");

        helloThread.start();
        hiThread.start();
        cioThread.start();
        buonGiornoThread.start();



    }



}
