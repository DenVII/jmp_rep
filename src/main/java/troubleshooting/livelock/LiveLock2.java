package troubleshooting.livelock;


import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LiveLock2 {

    private static Lock lock = new ReentrantLock();
    private static Lock lock2 = new ReentrantLock();

    private static Thread thread1;
    private static Thread thread2;


    public static void downloadFile() {

        lock.lock();

        try {
            if(Thread.interrupted()) {
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {

        }

        int a = 1;
        while (!Thread.currentThread().isInterrupted()) {
            a= a+a++;
        }

        lock.unlock();
        downloadFile2();

    }

    public static void downloadFile2() {

        lock2.lock();

        Thread threadToNotify = null;

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {

        }

        if (Thread.currentThread() != thread1 && thread1 != null) {
            threadToNotify = thread1;
        } else if(Thread.currentThread() != thread2 && thread2 != null) {
            threadToNotify = thread2;
        }

        while (threadToNotify == null || !threadToNotify.isInterrupted()) {
            if(threadToNotify != null) {
                threadToNotify.interrupt();
            }
        }

        lock2.unlock();
        downloadFile();

    }





    public static void main(String[] args) {

        thread1 = new Thread(()->{
            LiveLock2.downloadFile();
        });

        thread2 = new Thread(()->{
            LiveLock2.downloadFile2();
        });

        thread1.start();
        thread2.start();
    }

}
