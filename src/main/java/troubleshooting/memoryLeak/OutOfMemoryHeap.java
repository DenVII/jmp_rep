package troubleshooting.memoryLeak;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class OutOfMemoryHeap {

    public final static Map<Integer, String> map = new ConcurrentHashMap<Integer, String>() {{
        put(1, "http://www.oracle.com/");
        put(2, "http://stackoverflow.com/");
        put(3, "https://habrahabr.ru/");
        put(4, "https://ru.wikipedia.org");
    }};


    public static void main(String[] args) throws Exception {

        System.out.println("Start");

        for (int i = 0; i < 100000000; i++) {

            String str = new String("Вместо принудительного завершения потока применяется схема, в которой каждый поток сам ответственен за своё завершение. " +
                    "Поток может остановиться либо тогда, когда он закончит выполнение метода run(), (main() — для главного потока) либо по сигналу из другого потока." +
                    " Причем как реагировать на такой сигнал — дело, опять же, самого потока. Получив его, поток может выполнить некоторые операции и завершить выполнение," +
                    " а может и вовсе его проигнорировать и продолжить выполняться. Описание реакции на сигнал завершения потока лежит на плечах программиста. Java имеет встроенный механизм оповещения потока, который называется Interruption (прерывание, вмешательство), и скоро мы его рассмотрим, но сначала посмотрите на следующую программку:" +
                    "" +
                    "Incremenator — поток, который каждую секунду прибавляет или вычитает единицу из" +
                    " значения статической переменной Program.mValue. Incremenator содержит два закрытых поля – mIsIncrement и mFinish. То, какое действие выполняется, " +
                    "определяется булевой переменной mIsIncrement — если оно равно true, то выполняется прибавление единицы, иначе — вычитание. А завершение потока происходит, " +
                    "когда значение mFinish становится равно true."
            );

            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {

                    System.out.println(str);
                    try {
                        getConnection();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                public void getConnection() throws Exception {

                    int t = 1 + new Random(4).nextInt();
                    String cite = map.get(t);
                    if(cite == null) {
                        cite = "http://www.oracle.com/";
                    }
                    URL oracle = new URL(cite);
                    URLConnection yc = oracle.openConnection();
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            yc.getInputStream()));
                    String inputLine;
                    while ((inputLine = in.readLine()) != null)
                        System.out.println(new  String(inputLine));
                    in.close();
                }


            });

//            t.setDaemon(true);
            t.start();


            System.out.println(new String(i + " " + str));

        }

    }


}
