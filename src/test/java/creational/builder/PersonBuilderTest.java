package creational.builder;

import org.junit.Assert;
import org.junit.Test;
import patterns.creational.builder.Person;

import java.util.Date;

public class PersonBuilderTest {

    @Test
    public void personShouldBeCreated() {

        String name = "Denys";
        Date birthDate = new Date();
        String streetAddress = "Spusk Tolliati 17";
        int age = 24;


        Person Denys =   Person.builder().age(age)
                        .birthDate(birthDate)
                        .streetAddress(streetAddress)
                        .name(name).build();

        Assert.assertEquals(name, Denys.getName());
        Assert.assertEquals(birthDate, Denys.getBirthDate());
        Assert.assertEquals(streetAddress, Denys.getStreetAddress());
        Assert.assertEquals(age, Denys.getAge());

    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowExceptionIfNotAllFieldsFilled() {

        String name = "Denys";
        Date birthDate = new Date();
        String streetAddress = "Spusk Tolliati 17";
        int age = 24;


        Person Denys = Person.builder().age(age)
                .birthDate(birthDate)
                .streetAddress(streetAddress)
                .name(name).build();

        Person Denys2 = Person.builder().age(age)
                .birthDate(birthDate)
                .name(name).build();

    }

}
