package structural.adapter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import patterns.structural.adapter.ListAdapter;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Test class for ListAdapter class
 */
public class ListAdapterTest {


    protected ListAdapter arrayListAdapter;
    protected ListAdapter linkedListAdapter;

    @Before
    public void instantiateAdapters() {
        ArrayList<String> arrayList = new ArrayList<>();
        LinkedList<String> linkedList = new LinkedList<>();

        arrayListAdapter = new ListAdapter(arrayList);
        linkedListAdapter = new ListAdapter(linkedList);
    }


    @Test
    public void shouldGetElementFromTop() {

        //given
        String one = "one";
        String two = "two";

        arrayListAdapter.push(one);
        linkedListAdapter.push(one);

        arrayListAdapter.push(two);
        linkedListAdapter.push(two);

        Assert.assertEquals(two, arrayListAdapter.pop());
        Assert.assertEquals(two, linkedListAdapter.pop());

        Assert.assertEquals(one, arrayListAdapter.pop());
        Assert.assertEquals(one, linkedListAdapter.pop());

    }

    @Test
    public void shouldAddElement() {

        //given
        String one = "one";
        String two = "two";

        arrayListAdapter.push(one);
        linkedListAdapter.push(one);

        Assert.assertEquals(1, arrayListAdapter.size());
        Assert.assertEquals(1, linkedListAdapter.size());

    }

}
