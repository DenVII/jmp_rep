package structural.composite.FileSystem;


import org.junit.Assert;
import org.junit.Test;
import patterns.structural.composite.FileSystem.Directory;
import patterns.structural.composite.FileSystem.File;

public class CompositeFSTest {

    @Test
    public void shouldListAllFiles() {

        Directory mainDir = new Directory("Dir");
        Directory A = new Directory("A");
        Directory AA = new Directory("AA");
        Directory AB = new Directory("AB");
        Directory B = new Directory("B");

        File file1 = new File("File1", 300);
        File file2 = new File("File2", 500);
        File file3 = new File("File3", 1300);
        File file4 = new File("File4", 2200);
        File file5 = new File("File5", 900);
        File file6 = new File("File6", 450);
        File file7 = new File("File7", 100);

        mainDir.add(A);
        mainDir.add(B);
        mainDir.add(file1);
        mainDir.add(file2);

        A.add(AA);
        A.add(AB);
        A.add(file3);

        AA.add(file4);
        AA.add(file5);

        AB.add(file6);
        AB.add(file7);

        mainDir.list();

        Assert.assertEquals(mainDir.size(), 5750);
        Assert.assertEquals(A.size(), 4950);
        Assert.assertEquals(AA.size(), 3100);
        Assert.assertEquals(AB.size(), 550);
        Assert.assertEquals(B.size(), 0);

    }

}
