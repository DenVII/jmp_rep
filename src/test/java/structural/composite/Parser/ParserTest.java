package structural.composite.Parser;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import patterns.structural.composite.Parser.Element;
import patterns.structural.composite.Parser.XMLParser;

import java.io.File;
import java.nio.file.Paths;

public class ParserTest {

    XMLParser parser = new XMLParser();

    @Before
    public void getFile() {

        try {

            String fileName = Paths.get(getClass().getResource("/parserExample.xml").toURI()).toString();
            File fXmlFile = new File(fileName);
            parser.readFile(fXmlFile);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void shouldCreateMemoryStructure() {

        Element parent = parser.createMemoryStructure();
        parent.print();
        Assert.assertEquals(10, parent.getElementsCount());

    }

}
