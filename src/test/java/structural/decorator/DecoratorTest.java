package structural.decorator;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import patterns.structural.decorator.PersonInputStream;
import patterns.structural.decorator.PersonOutputStream;

import java.io.*;
import java.nio.file.Paths;

public class DecoratorTest {

    private File file;

    @Before
    public void getFile() {


        try {

            String fileName = Paths.get(getClass().getResource("/decorator.txt").toURI()).toString();
            this.file = new File(fileName);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testWritePerson() throws IOException{

        String person = "Denys";
        PersonOutputStream personOutputStream = new PersonOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
        personOutputStream.writePerson(person);
        personOutputStream.flush();
        personOutputStream.close();

        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        StringBuilder stringBuilder = new StringBuilder();

        int c = bufferedReader.read();

        while(c != -1) {


            stringBuilder.append((char)c);
            c = bufferedReader.read();
        }

        Assert.assertEquals("denys", stringBuilder.toString());

        bufferedReader.close();

    }

    @Test
    public void testReadPerson() throws IOException {

        String person = "Denys";

        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));

        bufferedOutputStream.write(person.getBytes());
        bufferedOutputStream.flush();
        bufferedOutputStream.close();

        PersonInputStream personInputStream = new PersonInputStream(new BufferedInputStream(new FileInputStream(file)));
        String name = personInputStream.readPerson();
        personInputStream.close();

        Assert.assertEquals(person.toLowerCase(), name);
    }


}
