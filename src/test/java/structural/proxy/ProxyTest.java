package structural.proxy;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import patterns.creational.abstract_factory.Person;
import patterns.structural.proxy.DataGetter;
import patterns.structural.proxy.PropertyDataGetterimpl;
import patterns.structural.proxy.ProxyPropertyGetter;

import java.io.File;
import java.io.FileReader;
import java.nio.file.Paths;
import java.util.Properties;


public class ProxyTest {

    private Properties p;
    private String name;

    @Before
    public void getProp() throws Exception{

        p = new Properties();
        name = "Vasya";
        String fileName = Paths.get(getClass().getResource("/person.properties").toURI()).toString();
        p.load(new FileReader(new File(fileName)));
        System.setProperties(p);
    }


    @Test
    public void shouldGetSamePersonOutOfProperties() {

        DataGetter dataGetter = new PropertyDataGetterimpl();
        ProxyPropertyGetter proxyPropertyGetter = new ProxyPropertyGetter(dataGetter);

        Person person = proxyPropertyGetter.readPerson(name);
        Assert.assertEquals(person.getName(), name);
        Assert.assertEquals(person.getAddress(), "Moscow");

        Person person1 = proxyPropertyGetter.readPerson(name);
        Assert.assertEquals(person, person1);

    }

    @Test
    public void shouldNotGetSamePersonOutOfProperties() {

        DataGetter dataGetter = new PropertyDataGetterimpl();

        Person person = dataGetter.readPerson(name);
        Person person1 = dataGetter.readPerson(name);
        Assert.assertNotEquals(person, person1);

    }


}
